binance_api_key = 'Rdrt4YNs1C9D3BQjLhtS6X90t7ENG5x2qupRUSRUS94RQqpl6wv41Kd4VR1A5uUY'
binance_secret_key = 'GS7tBNqlYx0GiXmYEcyug18zdhgYLPlQfVsqv95lzvQHhJU32pdQEKuBMBplQ1fl'
binance_mapper = {}
kraken_exchanges = {}
binance_exchanges = {}

def get_aggregation():
    response = {}
    for kraken_pair, kraken_info in kraken_exchanges.items():
        for binance_pair, binance_info in binance_exchanges.items():
            if (kraken_pair == binance_pair):
                print('pair!!!')
                response[kraken_pair] = {
                    'bid': round((float(kraken_info['b']) + float(binance_info['b']))/2, 4),
                    'ask': round((float(kraken_info['a']) + float(binance_info['a']))/2, 4)
                }
            elif (kraken_pair != binance_pair and kraken_info != None):
                response[kraken_pair] = {
                    'bid': round(float(kraken_info['b']), 4),
                    'ask': round(float(kraken_info['a']), 4)
                }

    for binance_pair, binance_info in binance_exchanges.items():
        for kraken_pair, kraken_info in kraken_exchanges.items():
            if (kraken_pair == binance_pair):
                print('pair!!!')
                response[kraken_pair] = {
                    'bid': round((float(kraken_info['b']) + float(binance_info['b']))/2, 4),
                    'ask': round((float(kraken_info['a']) + float(binance_info['a']))/2, 4)
                }
            elif (kraken_pair != binance_pair and binance_info != None):
                response[binance_pair] = {
                    'bid': round(float(binance_info['b']), 4),
                    'ask': round(float(binance_info['a']), 4)
                }
    return response

def get_aggr_exchange(exchange_key):
    response = {}
    if exchange_key == 'kraken':
        for kraken_pair, kraken_info in kraken_exchanges.items():
            response[kraken_pair] = {
                'bid': round(float(kraken_info['b']), 4),
                'ask': round(float(kraken_info['a']), 4)
            }
    elif exchange_key == 'binance':
        for binance_pair, binance_info in binance_exchanges.items():
            response[binance_pair] = {
                'bid': round(float(binance_info['b']), 4),
                'ask': round(float(binance_info['a']), 4)
            }
    return response

def get_aggr_pair_exchange(pair, exchange_key):
    response = {}
    if exchange_key == 'kraken':
        for kraken_pair, kraken_info in kraken_exchanges.items():
            if kraken_pair == pair:
                response[kraken_pair] = {
                    'bid': round(float(kraken_info['b']), 4),
                    'ask': round(float(kraken_info['a']), 4)
                }
    elif exchange_key == 'binance':
        for binance_pair, binance_info in binance_exchanges.items():
            if binance_pair == pair:
                response[binance_pair] = {
                    'bid': round(float(binance_info['b']), 4),
                    'ask': round(float(binance_info['a']), 4)
                }
    return response