FROM python:3.7-slim

MAINTAINER Julia Ivanova

RUN pip install --upgrade pip

COPY ./requirements.txt /opt/requirements.txt

RUN apt-get update && apt-get install -y git

RUN pip install --no-cache-dir -r /opt/requirements.txt

WORKDIR /opt/app/

EXPOSE 5000

VOLUME /opt/app

CMD ["python", "/opt/app/app.py"]
