from flask import Flask, request
import exchanges.binance_exchanges.binance
import threading
from exchanges.kraken_exchanges.kraken import run_socket
import config
import json


app = Flask(__name__)

@app.route('/get-price')
def get_price():
    if request.args.get('pair') != None and request.args.get('exchange_key') != None:
        response = {'data': json.dumps(config.get_aggr_pair_exchange(request.args.get('pair').upper(), request.args.get('exchange_key')))}
    elif request.args.get('exchange_key') != None:
        response = {'data': json.dumps(config.get_aggr_exchange(request.args.get('exchange_key')))}
    else:
        response = {'data': json.dumps(config.get_aggregation())}

    print(response)
    return response


if __name__ == '__main__':
    exchanges.binance_exchanges.binance.create_mapper()
    thread = threading.Thread(target=run_socket)
    thread.start()
    thread_binance = threading.Thread(target=exchanges.binance_exchanges.binance.start_listen)
    thread_binance.start()
    app.run(host='0.0.0.0')

