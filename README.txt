Open a terminal at the root of the project path.
For building the project use command:
```docker build -t test-task .  ```

Then use the command:
``` docker run -v $(pwd):/opt/app -p '5000:5000' --rm  test-task ```



if you want to get the price for the current cryptoexchange, you can use this endpoint:
``` GET /get-price ```

Parameters:
    exchange_key - the name of the exchange you are interested in. two expected values: 'binance' or 'kraken' (optional parameter)
    pair - currency pair name (optional parameter)
    




Task:
Тестовое задание на позицию Back-End разработчика

Задача:

Нужно создать небольшое приложение(API сервис), с помощью которого можно будет получать цены на текущий момент с двух топовых крипто-бирж(binance, kraken).

Нужно разработать интерфейс, который позволит получать данные по любой валютной паре с двух указанных бирж.

Должно быть два фильтра или параметра(пара - nullable, биржа - nullable).
Если не указана пара и биржа, то показывать все цены, которые есть на двух биржах, с учетом агрегации.
Если указана биржа - показывать все цены с этой биржи.
Если указана биржа и пара, то отображать цены по конкретной паре.

Для получения цен нужно использовать веб-сокет соединение.
Ходить по REST API не подходит, выполнять запрос в момент выполнения запроса на API сервисе - тоже не подходит.

Выполнить текущую задачу нужно на Python 3x, с использованием Django или Flask API, или любого другого  REST API фреймворка.

Приложение должно деплоиться с помощью Docker, базу данных прикручивать не стоит, сохранять там ничего не нужно.

Сделанное тестовое задание нужно залить на github в публичный репозиторий и выслать ссылку.


Некоторая информация по предметной области

На бирже есть набор торговых пар, по которым идёт торговля.
Например, на бирже “binance” - есть торговые пары, как BTC_USDT, ETH_USDT, etc.

По каждой из них, есть цена покупки и продажи. Нужно найти сред.значение между этими двумя значениями по каждой паре.


Нужно уметь получать цены по всем торговым парам, которые есть на бирже(по АПИ эта информация доступна)
Нужно учитывать, что название пары на каждой из бирж может отличаться, нужно название пары привести к общему формату и транслировать этот формат