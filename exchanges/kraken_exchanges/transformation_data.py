def trans_pair(data):
    pairs = []
    items = []
    size = 185
    for item in data:
        items.append(data[item])

    for pair in items:
        if 'wsname' in pair:
            pairs.append(pair['wsname'])

    new_pairs = []
    for i in range(0, len(pairs), size):
        new_pairs.append(pairs[i : i + size])

    return new_pairs

def trans_response(data):
    key = data[-1].replace('/', '_')
    body = {}
    body['a'] = data[1]['a'][0]
    body['b'] = data[1]['b'][0]
    body['key'] = key
    print(body)

    return body