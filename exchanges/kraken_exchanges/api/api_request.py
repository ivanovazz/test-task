import requests

# private query nonce
import time

# private query signing
import urllib.parse
import hashlib
import hmac
import base64
import json


class ApiRequest(object):
    def __init__(self, key="", secret=""):
        self.key = key
        self.secret = secret
        self.uri = 'https://api.kraken.com'

    def _nonce(self):
        return int(1000 * time.time())

    def _sign(self, data, urlpath):
        postdata = urllib.parse.urlencode(data)

        # Unicode-objects must be encoded before hashing
        encoded = (str(data['nonce']) + postdata).encode()
        message = urlpath.encode() + hashlib.sha256(encoded).digest()

        signature = hmac.new(base64.b64decode(self.secret),
                             message, hashlib.sha512)
        sigdigest = base64.b64encode(signature.digest())

        return sigdigest.decode()

    def get_pair(self):
        end_point = "/0/public/AssetPairs"
        data = {}
        data['nonce'] = self._nonce()
        headers = {
            'API-KEY': self.key,
            'API-Sign': self._sign(data, end_point),
        }
        response = requests.post(self.uri + end_point, data=data, headers=headers)

        return json.loads(response.content)