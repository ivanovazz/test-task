from exchanges.kraken_exchanges.api.api_request import ApiRequest
from exchanges.kraken_exchanges.transformation_data import *
import config
import json
import websocket

api_key = "8sOB2UYEbBdt+LANlTNqu+3/5KVvbYo4Qco5LPA9lGUjWJHxSbRKHmXg"
api_secret_key = "0zGSMJHjBt1Of8AtSy1Oe7dsy8cO/ziO6UUp6BlN4iBbApbi3FibXpwnHOoeiBpVyssSA7lvwiK1tRue2W4Kvw=="
api = ApiRequest(key=api_key, secret=api_secret_key)

print(trans_pair(api.get_pair()['result']))

def on_open(ws):
    print('connected')

    pairs = trans_pair(api.get_pair()['result'])

    for pair in pairs:
        data = {
              "event": "subscribe",
              "pair": pair,
              "subscription": {
                "name": "ticker"
              }
            }
        payload = json.dumps(data, separators=(',', ':'))
        ws.send(payload)

    print('Payload sent')

def on_message(ws, event):
    if 'ticker' in event and 'subscribed' not in event:
        item = trans_response(json.loads(event))
        config.kraken_exchanges[item['key']] = {'a': item['a'], 'b': item['b']}

def on_error(ws, err):
    print('Received error: %s' % err)

def create_ws():
    return websocket.WebSocketApp(
        'wss://ws.kraken.com',
        on_open=on_open,
        on_message=on_message,
        on_error=on_error,
    )

def run_socket():
    ws = create_ws()
    ws.run_forever()


# thread = threading.Thread(target=run_socket)
# thread.start()

print('Thread is starting')