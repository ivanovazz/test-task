import requests
import json

class ApiRequest(object):
    def __init__(self):
        self.uri = 'https://api.binance.com'

    def get_exchange_info(self):
        end_point = "/api/v1/exchangeInfo"
        response = requests.get(self.uri + end_point)

        return json.loads(response.content)