def trans_response(data):
    members = [attr for attr in dir(data) if not callable(attr) and not attr.startswith("__")]
    body = {}
    for member_def in members:
        val_str = str(getattr(data, member_def))
        if member_def == 'symbol':
            body['key'] = val_str
        if  member_def == 'bestAskPrice':
            body['a'] = val_str
        if member_def == 'bestBidPrice':
            body['b'] = val_str
    print(body)

    return body

