import logging
import config
from binance_f import SubscriptionClient
from binance_f.model import *
from binance_f.exception.binanceapiexception import BinanceApiException
from exchanges.binance_exchanges.transformation_data import *
from exchanges.binance_exchanges.api.api_request import ApiRequest

logger = logging.getLogger("binance-futures")
logger.setLevel(level=logging.INFO)
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger.addHandler(handler)


sub_client = SubscriptionClient(api_key=config.binance_api_key, secret_key=config.binance_secret_key)


def callback(data_type: 'SubscribeMessageType', event: 'any'):
    if data_type == SubscribeMessageType.RESPONSE:
        print("Event ID: ", event)
    elif data_type == SubscribeMessageType.PAYLOAD:
        item = trans_response(event)
        config.binance_exchanges[config.binance_mapper[item['key']]] = {'a': item['a'], 'b': item['b']}
        print('')
    else:
        print("Unknown Data:")
    print()


def error(e: 'BinanceApiException'):
    print(e.error_code + e.error_message)

def start_listen():
    sub_client.subscribe_all_bookticker_event(callback, error)

def create_mapper():
    api = ApiRequest()
    response = api.get_exchange_info()

    for item in response['symbols']:
        config.binance_mapper[item['symbol']] = item['baseAsset'] + '_' + item['quoteAsset']